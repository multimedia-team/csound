<CsoundSynthesizer>
  <CsOptions>
  -odac
  </CsOptions>
  <CsInstruments>r
sr     =        44100
;kr     =        4410
ksmps  =        256
 0dbfs = 4
    
    instr 1       
    				;xamp	;irise		;idur	;idec
    	a2 linen  	p4,		0.01 * p3, 	p3, 		0.2 * p3  
    				;xamp, 	;kcps	xcar			xmod		kndx		ifn 	iphs
    	a1 foscil 	a2, 		p5, 		1,   		p7, 		p6, 		1, 	3
     out a1*0.6
    endin
    
    instr 2
    	a1	oscil	p4,	p5,	1
    	a2 	harmon	a1,	p5,	0.1,	p5 * 3,	p5 * 5,	1,	100, 0.2
    	out (a1*0.4+a2*0.6)*1.4
    endin
    	
    
  </CsInstruments>
  <CsScore>
    f 1 0 4096 10 1 0.5 0.25 0.25 0.10 0.05 0.05 0.025 0.0125
    f 2 0 4096 10 1
#define TIME # 0.25 #
#define freq # 146 #


#define T # $freq #
#define Am # $freq * 17 / 16 #
#define AM # $freq * 9/8 #
#define E # $freq #

 ;inst  	time  		dur					amp 		note			p6     	p7
 m parta
{2 O
i1          [$TIME * $O * 2]           $TIME                  2           $T             1      3/2
}
e
    </CsScore>
</CsoundSynthesizer>
