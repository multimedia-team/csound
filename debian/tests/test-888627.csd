<CsoundSynthesizer>
  <CsOptions>
  -odac
  </CsOptions>
  <CsInstruments>r
sr     =        44100
;kr     =        4410
ksmps  =        256
 0dbfs = 40
    
    instr 1
                    ;xamp,     ;kcps    xcar         xmod        kndx        ifn     iphs
        a1 foscil  p4,         p5,     1,           p7,         p6,         1,     0
     out a1
    endin
        
    
  </CsInstruments>
  <CsScore>
    f 1 0 4096 10 1 0.5 0.25 0.25 0.10 0.05 0.05 0.025 0.0125
    
;1          2           3                   4           5               6           7
 ;inst      time        dur                 amp         note             p6          p7
m foo
i1          0          0.15               2           120               1.5         6  
i1          +           .                  .          139  1.0         . 
i1          +           .                  .          67  1.9         . 
i1          +           .                  .          110  1.0         .
s
n foo
e
    </CsScore>
</CsoundSynthesizer>
