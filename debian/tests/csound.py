#!/usr/bin/env python3

import ctcsound
import sys

c = ctcsound.Csound()
c.compile_(sys.argv)
c.perform()
c.stop()
