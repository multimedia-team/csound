#!/usr/bin/env lua
require "luaCsnd6"

local c = luaCsnd6.Csound()
c:Compile(arg[1])
c:Perform()
c:Stop()
